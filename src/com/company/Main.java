package com.company;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class Main {

    static Scanner scanner = new Scanner(System.in);

    static int LastId=0;

    static class Furniture{
        enum ClassesOfFurnit {
            Economy,
            Middle,
            Premium,
            Luxury,
            DeLux
        }

        int Id;
        String Name;
        double Weight;
        ClassesOfFurnit ClassOfFurniture;
        double CustomerRating;

        Furniture(String Name, double Weight, ClassesOfFurnit ClassOfFurniture, double CustomerRating)
        {
            this.Name = Name;
            this.Weight = Weight;
            this.ClassOfFurniture = ClassOfFurniture;
            this.CustomerRating = CustomerRating;
        }
    }

    //----------------------System Functions---------------------//

    static Furniture[] ResizeArray(Furniture[] oldFurnitures, int newLength)
    {
        Furniture[] newFurnitures = new Furniture[newLength];

        int minLength = newLength < oldFurnitures.length? newLength: oldFurnitures.length;

        for (int i=0;i<minLength;i++)
        {
            newFurnitures[i]=oldFurnitures[i];
        }
        return newFurnitures;
    }

    static void PrintMenu()
    {
        System.out.println("1. Add new furniture");
        System.out.println("2. Print all furnitures on screen");
        System.out.println("3. Print all furnitures on file");
        System.out.println("4. Sort furnitures by customer rating");
        System.out.println("5. Print furnitures between min and max weight");
        System.out.println("0. Exit");
    }

    static int InputMenuPoint()
    {
        System.out.print("Input menu point: ");
        return scanner.nextInt();
    }

    static void Print50Enters()
    {
        for (int i=0;i<50;i++)
        {
            System.out.println();
        }
    }
    //----------------------System Functions---------------------//

    //----------------------CRUD Functions---------------------//
    static Furniture[] AddToEnd(Furniture[] furnitures, Furniture furniture)
    {
        LastId++;
        furniture.Id = LastId;

        if(furnitures==null)
        {
            furnitures = new Furniture[1];
        }
        else
        {
            furnitures = ResizeArray(furnitures, furnitures.length+1);
        }
        furnitures[furnitures.length-1] = furniture;

        return furnitures;
    }
    //----------------------CRUD Functions---------------------//

    //----------------------Ext Functions---------------------//
    static void SortByCustomerRating(Furniture[] furnitures, boolean ASC)
    {
        boolean sort;
        Furniture temp;
        int offset=0;

        do
        {
            sort=true;

            for (int i=0;i<furnitures.length-1-offset;i++)
            {
                boolean condition;
                if(ASC)
                {
                    condition = furnitures[i+1].CustomerRating<furnitures[i].CustomerRating;
                }
                else
                {
                    condition = furnitures[i+1].CustomerRating>furnitures[i].CustomerRating;
                }
                if(condition)
                {
                    sort=false;
                    temp=furnitures[i];
                    furnitures[i]=furnitures[i+1];
                    furnitures[i+1]=temp;
                }
            }

            offset++;
        }
        while(!sort);
    }

    static void PrintSingle(Furniture furniture)
    {
        System.out.printf("%-4d%-12s%-10f%-10s%-10f\n", furniture.Id, furniture.Name, furniture.Weight, furniture.ClassOfFurniture, furniture.CustomerRating);
    }

    static void PrintAll(Furniture[] furnitures)
    {
        System.out.printf("%-4s%-12s%-10s%-10s%-10s\n","Id","Name","Weight","ClassOfFurniture","CustomerRating");

        if(furnitures==null)
        {
            System.out.println("Array is empty");
        }
        else if(furnitures.length==0)
        {
            System.out.println("Array is empty");
        }
        else
        {
            for (int i=0;i<furnitures.length;i++)
            {
                PrintSingle(furnitures[i]);
            }
        }
        System.out.println("===============================================================");
    }

    static Furniture CreatFurniture()
    {
        System.out.print("Input Name: ");
        String name = scanner.next();

        System.out.print("Input Weight: ");
        double weight = scanner.nextDouble();

        System.out.print("Input Class of furniture: ");
        Furniture.ClassesOfFurnit classOfFurniture = Furniture.ClassesOfFurnit.valueOf(scanner.next());

        System.out.print("Input Customer rating: ");
        double customerRating = scanner.nextDouble();

        return new Furniture(name, weight, classOfFurniture, customerRating);
    }

    static double GetMinWeight(Furniture[] furnitures)
    {
        double minWeight = furnitures[0].Weight;

        for (int i=0;i<furnitures.length;i++)
        {
            if(furnitures[i].Weight<minWeight)
            {
                minWeight = furnitures[i].Weight;
            }
        }

        return minWeight;
    }
    static double GetMaxWeight(Furniture[] furnitures)
    {
        double maxWeight = furnitures[0].Weight;

        for (int i=0;i<furnitures.length;i++)
        {
            if(furnitures[i].Weight>maxWeight)
            {
                maxWeight = furnitures[i].Weight;
            }
        }

        return maxWeight;
    }

    static Furniture[] GetFurnituresBetweenMaxMinWeight(Furniture[] furnitures, double minWeight, double maxWeight)
    {
        Furniture[] foundFurnitures = null;

        for (int i=0;i<furnitures.length;i++)
        {
            if (furnitures[i].Weight>=minWeight && furnitures[i].Weight<=maxWeight)
            {
                foundFurnitures = AddToEnd(foundFurnitures, furnitures[i]);
            }
        }

        return foundFurnitures;
    }
    //----------------------Ext Functions---------------------//

    //----------------------File Functions--------------------//
    static void PrintAllToFile(Furniture[] furnitures, String filename) throws FileNotFoundException {
        PrintStream stream = new PrintStream(filename);

        stream.printf("%-4s%-12s%-10s%-10s%-10s","Id","Name","Weight","ClassOfFurniture","CustomerRating");
        stream.println();

        if(furnitures==null)
        {
            System.out.println("Array is empty");
        }
        else if(furnitures.length==0)
        {
            System.out.println("Array is empty");
        }
        else
        {
            for (int i=0;i<furnitures.length;i++)
            {
                stream.printf("%-4d%-12s%-10f%-10s%-10f",furnitures[i].Id,furnitures[i].Name,furnitures[i].Weight,furnitures[i].ClassOfFurniture,furnitures[i].CustomerRating);
                stream.println();
            }
        }

        stream.close();
    }
    //----------------------File Functions--------------------//
    public static void main(String[] args) throws IOException {
        Furniture[] furnitures = null;
        int menuPoint;

        Furniture furniture;
        int id;
        boolean ASC;
        String filename;

        do
        {
            Print50Enters();
            PrintAll(furnitures);
            PrintMenu();

            menuPoint=InputMenuPoint();

            switch (menuPoint)
            {
                case 1:
                    furniture = CreatFurniture();
                    furnitures = AddToEnd(furnitures, furniture);
                    break;

                case 2:
                    PrintAll(furnitures);
                    break;

                case 3:
                    System.out.print("Input file name: ");
                    filename = scanner.next();

                    PrintAllToFile(furnitures, filename);
                    break;

                case 4:
                    System.out.print("In ascending order(true or false): ");
                    ASC=scanner.nextBoolean();

                    SortByCustomerRating(furnitures,ASC);
                    break;

                case 5:
                    System.out.println("Input min and max weights between "+GetMaxWeight(furnitures)+" and "+GetMinWeight(furnitures));

                    System.out.print("Input min: ");
                    double minWeight = scanner.nextDouble();

                    System.out.print("Input max: ");
                    double maxWeight = scanner.nextDouble();

                    PrintAll(GetFurnituresBetweenMaxMinWeight(furnitures, minWeight, maxWeight));
                    break;

                case 0:
                    System.out.println("Programm will be finished");
                    break;

                default:
                    System.out.println("Unknown menu point");
                    break;
            }
            System.in.read();
        }
        while(menuPoint!=0);
    }
}
